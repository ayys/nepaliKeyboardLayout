# nepaliKeyboardLayout
सुधारिएको नेपाली कुंजीपेटि रुपरेखा

## विस्थापन प्रक्रिया
```
	sudo su
	cp np /usr/share/X11/xkb/symbols/
	dpkg-reconfigure xkb-data
```

## सुधारहरु

* Shift + 0 = ण
* Shift + 1 = !
* Shift + 2 = @
* Shift + 3 = #
* Shift + 4 = $
* Shift + 5 = %
* Shift + 6 = ^
* Shift + 7 = &
* Shift + 8 = *
* Shift + 9 = ꣩

* c = च
* C = छ
